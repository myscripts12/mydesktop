# myDesktop

This repository contains my XFCE4 desktop configuration with my ZSH, neoVIM and Visual Studio Code configuration. The import script will import the current configuration.

## Supported OS

Currently only Manjaro is supported and tested.



# Import

The import script will install the base packages and import all configurations.

To run it:

```zsh
curl https://gitlab.com/myscripts12/mydesktop/-/raw/master/install.sh | bash
```
