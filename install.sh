#!/bin/bash/
# This script install a base desktop for Manjaro/ArchLinux based on XFCE4.

# Update system

sudo pacman -Syyu --noconfirm --quiet


# Install git and base-devel if necessary to install Paru (AUR package manager)

sudo pacman -S git base-devel --needed

# Install Paru
cd /tmp
git clone https://aur.archlinux.org/paru.git
cd paru 
makepkg -si
cd && rm /tmp/paru

# Install base programs

paru -S --noconfirm --quiet	--needed arduino \
							bmon \
							byobu \
							cura \
							curl \
							firefox \
							joplin-desktop \
							onlyoffice-desktopeditors \
							spotify \
							thunderbird \
							unzip \
							vlc \
							virt-manager \
							wget
							

# Remove programs

paru -Rnsc --noconfirm --quiet 	midori \
								mousepad \
								pamac-cli \
								pamac-gtk \
								parole \
								xfburn \
								xfce4-notes-plugin 

# Run myZSH script

curl https://gitlab.com/myscripts12/myzsh/-/raw/minimal/import.sh | bash 

# Run myNeoVIM script

#curl https://gitlab.com/myscripts12/myneovim/-/raw/master/import.sh | bash

# Run myVSC script

#curl https://gitlab.com/myscripts12/myvsc/-/raw/master/import.sh | bash

# Run myXFCE script

#curl https://gitlab.com/myscripts12/myxfce/-/raw/master/import.sh | bash

# Run myGesture scripts

#TODO

# Run myCPUgov script

#TODO

# Run Reboot script

#TODO
